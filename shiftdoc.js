$(document).ready(function (){
// initialization
  $('select').material_select();

  $('.datepicker').pickadate({
    selectMonths: true,
    selectYears: 15
  });

  var dateSubmit = $('#dateSubmit').val();
  var leadMentor;
  var otherMentor;
  var mentorList = [];
  var studentList = [];
  var studentsPresent;
  var studentName;
  var mentorName;
  var oneToOne = $('#oneToOne').val();
  var studentIssues = $('#studentIssues').val();
  var workbookIssues = $('#workbookIssues').val();
  var wentWell = $('#wentWell').val();
  var notGoWell = $('#notGoWell').val();

  $("#leadMentor").on( 'change', function(){
    leadMentor = $(this).val();
    mentorList = $("#otherMentor").val();
    mentorList.push(leadMentor)
    $('.otherMentor li').show();
    $('.otherMentor li:contains(' + leadMentor + ')').hide();
  });

  $("#otherMentor").on( 'change', function(){
    mentorList = $(this).val();
    if (leadMentor) {
      mentorList.push(leadMentor)
    };
    $('.leadMentor li').show()
    for(i = 0; i < mentorList.length; i++){
      $('.leadMentor li:contains(' + mentorList[i] + ')').hide()
    };
    $('.mentorName li').hide()
    for(i = 0; i < mentorList.length; i++){
      $('.mentorName li:contains(' + mentorList[i] + ')').show()
    };
  });

  $("#studentsPresent").on( 'change', function(){
    studentList = $(this).val();
    $('.studentName li').hide()
    for(i = 0; i < studentList.length; i++){
      $('.studentName li:contains(' + studentList[i] + ')').show()
    };
  });

  function validate(){
    if(
      $('#dateSubmit').val().length > 0 &&
      $('#leadMentor').val().length > 0 &&
      $('#otherMentor').val().length > 0 &&
      $('#studentsPresent').val().length > 0 &&
      $('#studentName').val().length > 0 &&
      $('#mentorName').val().length > 0 &&
      $('#oneToOne').val().length > 0 &&
      $('#wbId').val().length > 0 &&
      $('#wbQuestion').val().length > 0 &&
      $('#wbIssue').val().length > 0 &&
      $('#wentWell').val().length > 0 &&
      $('#notGoWell').val().length > 0) {
      $('#submitModal').openModal();
      }
    else {
      alert("Please fill in all the required fields (indicated by *)");
      }
  };

  $("#submit-btn").click(function(event){
    event.preventDefault()
    validate();
  });

  $("#addStudentIssue").click(function(){
    var studentName = $("#studentName").val();
    var mentorName = $("#mentorName").val();
    var oneToOne = $("#oneToOne").val();
    var addIssue = $("#addIssue").val();
    $("#studentIssues").append("<li class='list col m12'>"+ 'Name: ' + studentName + ',  Mentor Name: ' + mentorName +',  One-To-One: ' + oneToOne +',  Issue: ' + addIssue + '<button class="btn" id="remove-btn">-</button>' + "</li>");

  // reset values for select dropdown
    $("#studentName").val('');
    $("#mentorName").val('');
    $("#oneToOne").val('');
    $("#addIssue").val('');

  // reset select box
    $(".studentName > div > input").val('Select Student')
    $(".mentorName > div > input").val('Select Mentor')
    $(".oneToOne > div > input").val('One-to-one?')

  // remove item and remove button in list
    $('.list').click(function() {
      $(this).remove();
    });
  });

  $("#addWBIssue").click(function(){
    var addWorkbookId = $("#wbId").val();
    var addWorkbookIssue = $("#wbIssue").val();
    var addWorkbookQuestion = $("#wbQuestion").val();
    if (addWorkbookId.length>0 | addWorkbookIssue.length>0 | addWorkbookQuestion.length>0){
      $("#workbookIssues").append("<li class= 'list' col m12>"+ 'Workbook Id: ' + addWorkbookId + ',  Question #: ' + addWorkbookQuestion +',  Question Issue: ' + addWorkbookIssue + '<button class="btn" id="remove-btn">-</button>' +"</li>");
      $("#wbId").val("");
      $("#wbQuestion").val("");
      $("#wbIssue").val("");
    }
  // remove item and remove button in list
    $('.list').click(function() {
      $(this).remove();
    });
  });

});
